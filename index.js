import { 
    print_web_doc, 
    retrieve_and_save,
    print_web_links
} from './Classes/Retriever'

import {
    get_jobs, 
    get_ids,
    parse_job
} from './Classes/Scrapper'

import { post_job } from './Classes/Reporter'

import fs from 'fs'


const root = `https://www.indeed.com.mx`
const path = `/jobs?q=python`
const url = `${root}${path}`

const init = file => {
    retrieve_and_save(url, file)
}


const doc = 'data/python_jobs.txt'
const explore = doc => {

    const texts = print_web_doc(doc)
    const links = print_web_links(doc)
    const jobs = get_jobs(links)

    const ids = get_ids(jobs)
    const { id, link } = ids[0]
    const job_doc = `data/jobs/${id}.txt`

    if(fs.existsSync(job_doc)){
        const job = parse_job(job_doc)
        post_job(job)
    } 
    
    else { retrieve_and_save(`${root}${link}`, id) }

}


explore(doc)
