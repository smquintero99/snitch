import { Parser } from 'htmlparser2'
import fs from 'fs'


const title_parser = doc => {
    const state = {texts: []}
    new Parser({
        onopentag(name){ state.tag = name },
        ontext(text){ 
            const str = text.replace('\\n', '').replace('\\n', '')
            !['script', 'style'].includes(state.tag) 
            && str.trim().length > 1
                ?   state.texts = [ ...state.texts, str ]
                :   null 
        }
    }, {decodeEntities: true}).write(doc)
    return state.texts[0].split(' - ')[0]
}

const descriptor_parser = doc => {
    const state = {texts: []}
    new Parser({
        onopentag(name){ state.tag = name },
        ontext(text){ 
            const str = text.replace('\\n', '').replace('\\n', '')

            'li' === state.tag && str.trim().length > 1
                ?   state.texts = [ ...state.texts, str ]
                :   null 
        }
    }, {decodeEntities: true}).write(doc)

    return state.texts.slice(0,5)
}

const money_parser = doc => {
    const state = {texts: []}
    new Parser({
        onopentag(name){ state.tag = name },
        ontext(text){ 
            const str = text.replace('\\n', '').replace('\\n', '')

            text.includes('$') && str.trim().length > 1
                ?   state.texts = [ ...state.texts, str ]
                :   null 
        }
    }, {decodeEntities: true}).write(doc)

    return state.texts.map(s => 
        s.split(' ').filter(s => s.includes('$'))[0]
    )[0]
}



export const get_jobs = links => links
    .filter(link => link.includes('/company'))
    .filter(link => link.includes('?fccid'))
    .map(link => link.substring(0, link.lastIndexOf('?fccid')))
    .filter(link => link.includes('-'))


export const get_ids = jobs => jobs.map(job => ({
        id: job.substring(job.lastIndexOf('-') + 1, job.length),
        link: job
    }))


export const parse_job = doc =>  ({
    title: title_parser(fs.readFileSync(doc, 'utf8')),
    description: descriptor_parser(fs.readFileSync(doc, 'utf8')),
    salary: money_parser(fs.readFileSync(doc, 'utf8'))
})
