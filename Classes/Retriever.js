import { Parser } from 'htmlparser2'
import axios from 'axios'
import fs from 'fs'



const text_parser = doc => {
    const state = { texts: [] }
    new Parser({
        onopentag(name){ state.tag = name },
        ontext(text){ 
            const str = text.replace('\\n', '').replace('\\n', '')
            !['script', 'style'].includes(state.tag) 
            && str.trim().length > 1
                ?   state.texts = [ ...state.texts, str ]
                :   null 
        }
    }, {decodeEntities: true}).write(doc)
    return state.texts
}


const link_parser = doc => {
    const state = { links: [] }
    new Parser({
        onopentag(name, attribs) { 
            state.href = name === 'a' 
            && (attribs.href || attribs['\\nhref'])
                ? attribs.href || attribs['\\nhref'] 
                : ''
        },

        ontext(text){
            state.href 
            && state.href !== '\\"#\\"'
            && !state.href.substring(0,5).includes('"htt')
                ?   state.links = [
                        ...state.links,
                        `${
                            state.href.substring(
                                state.href.substring(0,2) === '\\"' ? 2 : 0,
                                state.href.lastIndexOf('"\\ntitle') + 1
                                    ?   state.href.lastIndexOf('"\\ntitle')
                                    :   state.href.length
                            )
                        }`, 
                    ]
                :   null 
        }
    }, {decodeEntities: true}).write(doc)

    state.links = [...new Set(state.links)].map(l => ['"', '\\'].includes(l[l.length-1]) ? l.substring(0, l.length -2) : l)
    return state.links
}


export const print_web_page = url => axios.get(url).then(({data}) => text_parser(data))
export const retrieve_and_save = (url, file) => axios.get(url)
    .then(({data}) => fs.writeFileSync(`data/jobs/${file}.txt`, JSON.stringify(data))).catch(console.log)

export const print_web_doc = doc =>  text_parser(fs.readFileSync(doc, 'utf8'))
export const print_web_links = doc => link_parser(fs.readFileSync(doc, 'utf8').replace(/\\n/g, ' '))
