import jobs from './jobs.json'

import { Parser } from 'htmlparser2'
import moment from 'moment'
import axios from 'axios'
import Slack from 'slack'

import fs from 'fs'

const KEYS = [
    'id',
    'type',
    'url',
    'created_at',
    'company',
    'company_url',
    'location',
    'title',
    'description',
    'how_to_apply',
    'company_logo'
] 

const token = 'xoxp-888676495456-877208005347-877318134627-267f2e96e8c0e4e1a1f0a7b6699c5565'
const bot = new Slack({token})
const channel = 'CS6FR3UPR'

const link_parser = (link) => {
    let state = ''
    new Parser({
        onopentag(name, attribs) { 
            name === 'a' && attribs.href
            ? state = attribs.href : null
        },
    }, {decodeEntities: true}).write(link)
    return state
}

const description_parser = (description) => {
    let state = ''
    new Parser({
        ontext(text){ state.length < 100 ? state += text : null }
    }, {decodeEntities: true}).write(description)
    return state.replace(/(\r\n|\n|\r)/gm, ' ')
}

const web_parser = (description) => {
    let state = false
    new Parser({
        onopentag(name){ state = name },
        ontext(text){ !['script', 'style'].includes(state) ? console.log(state, text) : null }
    }, {decodeEntities: true}).write(description)
}

const git_jobs = 'https://jobs.github.com/positions.json'
const git_file = 'jobs.json'

const retrieve_and_save = (url, file) => axios.get(url)
.then(({data}) => web_parser(data))
//.then(({data}) => fs.writeFileSync(file, JSON.stringify(data))).catch(console.log)


const post_msg = text => bot.chat.postMessage({channel: channel, text: text}).catch(console.log)

const keys = Object.keys(jobs[0])
const one_month_ago = moment().subtract(30, 'days')
const recent_jobs = jobs.filter(({ created_at}) => new Date(created_at) > one_month_ago)

//recent_jobs.map(({title}) => console.log(title))
//recent_jobs.map(({how_to_apply}, idx) => console.log(idx, link_parser(how_to_apply)))

//recent_jobs.map(({description}, idx) => console.log(idx, description_parser(description)))
//post_msg(description_parser(recent_jobs[45].description))

const indeed = "https://www.indeed.com.mx/ver-empleo?cmp=Reps-%26-Co&t=Data+engineer&jk=698dfc69e76a8302&sjdu=QwrRXKrqZ3CNX5W-O9jEvc04jWNo28apr20OUY0F8rdSC5a6-EPxkYmTIT83i-FQyrBUw-ZNhsb3_vILmG3HVB_mlcP1aGG4bSXE_d4tJzQ&tk=1dted0acd33ca000&adid=321338592&vjs=3"
const indeed_file = 'indeed.txt'

retrieve_and_save(indeed, indeed_file)
